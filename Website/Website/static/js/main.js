// JavaScript Document

/* Search Bar Sorting */
function empSearch(empType , empPosition) {
	$(document).ready(function(){
		var totalEmployees = $('.employees-wrapper').length;
		var extraEmp = totalEmployees + 1;
		$(empType).keyup(function(){
			var empSearch = $(empType).val().toUpperCase(); // Will Get the Value User will Search
			for(var i = 0 ; i < extraEmp ; i++)	{
				var employeeName = $('.employees-wrapper figcaption ' + empPosition + ':eq(' + i +')').text().toUpperCase(); // Get All Employee Name
				var searchIndex = employeeName.indexOf(empSearch); // Check If this fullfills the search result Selected
				if(searchIndex > -1) {
					$('.employees-wrapper:eq(' + i + ')').show();
				} else {
					$('.employees-wrapper:eq(' + i + ')').hide();
				}
			}	
		});
	});
}

empSearch(".search-employee" , "h2");
empSearch(".search-designation" , "p");
/* Search Bar Sorting End */

/* Open Feedback Modal */

$('.employees-wrapper figcaption button').click(function(){
	var getIndex = $(this).parents(':nth(2)').index();
	var getModalID = $('.employees-wrapper figure p:eq(' + getIndex + ')').text();
	var splitModalID = getModalID.split(":");
	var setModalID = $('.feedback-modal').prop("id" , splitModalID[1]);
	var employeeName = $('.employees-wrapper figcaption h2:eq(' + getIndex + ')').text();
	var employeePic = $('.employees-wrapper figure img:eq(' + getIndex + ')').attr('src');
	var employeeDesignation = $('.employees-wrapper figcaption p:eq(' + getIndex + ')').text();
	$('.modal-title').text(employeeName + "'s Ratings and Feedback");
	$('.profile-pic').html('<img src="' + employeePic + '" class="img-responsive" />');
	$('.profile-name').text(employeeName);
	$('.profile-designation').text(employeeDesignation);
	$('.ratings-section p span').text(employeeName);
	$(setModalID).modal();
});

/* Open Feedback Modal End */

/* Rating Sections and Feedback Section Toggle */
$('.feedback-section-inner').hide();
$('.feedback-button button').hide();
$('.star-button button').click(function(){
	$('.feedback-section-inner').show();
	$('.ratings-section-inner').hide();
	$('.feedback-button button').show();
	$('.star-button button').hide();
});
$('.feedback-button .back-btn').click(function(){
	$('.feedback-section-inner').hide();
	$('.ratings-section-inner').show();
	$('.feedback-button button').hide();
	$('.star-button button').show();
});
/* Rating Section and Feedback Section Toggle End */

/* Save the Ratings and Feedback to JSON */
$('.sub-btn').click(function() {
	var profileName = $('.profile-details h1').text();
	var overallRating = $('#overall-rating').val();
	var feedback = $('#feedback').val();
	var suggestion = $('#suggestion').val();
	$.ajax({
		type: "POST",
		url: "application/function.php",
		data: {task:"rating" , profileName:profileName , overallRating:overallRating , feedback:feedback , suggestion:suggestion},
		beforeSend : function() {
			$('#register').html('Registering...');
		},
		success: function(data) {
			$('#success-modal').modal();
		},
		
		error: function(error) {
			console.log(error);
		}
	});
});
/* Save the Ratings and Feedback to JSON End */